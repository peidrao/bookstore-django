from django import forms


from .models import Books


class BookModelForm(forms.ModelForm):

    class Meta:
        model = Books
        fields = ["title", 'author', 'genres', 'date']
        widgets = {
            "title": forms.TextInput(attrs={"placeholder": "Book's Title", 'class': "form-control-sm"}),
            "author": forms.TextInput(attrs={"placeholder": "Author's name", 'class': "form-control-sm"}),
            "genres": forms.Select(attrs={'class': "form-control"}),
            'date': forms.DateTimeInput(attrs={'class': "form-control"}),
            # 'slug': forms.TextInput(attrs={'placeholder': 'slugs', 'class': "form-control-sm"})
        }
