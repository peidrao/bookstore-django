from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.urls import reverse
from django import forms
# Create your models here.


class Author(models.Model):
    name = models.CharField(max_length=250)
    lastname = models.CharField(max_length=250)


class Books(models.Model):
    GENRES_CHOICES = (
        ("1", "Literatura de ficção"),
        ("2", "Poesia"),
        ("3", "Quadrinhos"),
        ("4", "Livros científicos"),
        ("5", "Religião")
    )

    title = models.CharField(max_length=240)
    author = models.CharField(max_length=240)

    genres = models.CharField(
        max_length=1, choices=GENRES_CHOICES, blank=False, null=False)
    date = models.DateField()
    slug = models.SlugField(null=True)
    created_at = models.DateField(auto_now=True)
    count = models.IntegerField(null=True, default=0)

    def get_absolute_url(self):
        return reverse("books", kwargs={"pk": self.pk})

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        return super(Books, self).save(*args, **kwargs)
