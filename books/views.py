from django.shortcuts import get_object_or_404, reverse
from django.utils import timezone
from django.db.models import F

from django.views.generic import (
    ListView, DetailView, CreateView, UpdateView, DeleteView)


from books.models import Books
from .forms import BookModelForm

# Create your views here.


class Index(ListView):
    model = Books
    template_name = 'home.html'
    context_object_name = 'books'
    paginate_by = 4


class BookListAllView(ListView):
    model = Books
    template_name = 'books/list-books.html'
    context_object_name = 'books'


class BookDetailView(DetailView):
    model = Books
    template_name = 'book-detail.html'
    #form_class = BookModelForm
    context_object_name = 'book'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        post = Books.objects.filter(slug=self.kwargs.get('slug'))
        post.update(count=F('count') + 1)

        context['time'] = timezone.now()

        return context


class CreateBookView(CreateView):
    template_name = 'crud/book_create.html'
    form_class = BookModelForm
    queryset = Books.objects.all()
    success_url = '/'

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)


class UpdateBookView(UpdateView):
    template_name = 'crud/book_update.html'
    form_class = BookModelForm
    success_url = '/'

    def get_object(self):
        id_ = self.kwargs.get('id')
        return get_object_or_404(Books, id=id_)

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)


class DeleteBookView(DeleteView):
    template_name = 'crud/book_delete.html'
    success_url = '/'

    def get_object(self):
        id_ = self.kwargs.get('id')
        return get_object_or_404(Books, id=id_)
