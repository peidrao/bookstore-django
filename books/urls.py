from django.urls import path
from .views import (
    Index,
    BookDetailView,
    CreateBookView,
    UpdateBookView,
    DeleteBookView,
    BookListAllView)

app_name = 'books'

urlpatterns = [
    path('', Index.as_view(), name='home'),
    path('books/', BookListAllView.as_view(), name='list-book'),
    path('create/', CreateBookView.as_view(), name='create-book'),
    path('update/<int:id>', UpdateBookView.as_view(), name='update-book'),
    path('delete/<int:id>', DeleteBookView.as_view(), name='delete-book'),
    path('<slug:slug>/', BookDetailView.as_view(), name='book-detail'),


]
